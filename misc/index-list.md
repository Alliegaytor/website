---
title: index-list
layout: index-list
---

# List of short blogs

{% for p in site.pages %}
  {% if p.category contains "post" %}
   * [{{ p.title }}]({{ p.url | absolute_url }})
  {% endif %}
{% endfor %}

# Other pages

{% for p in site.pages %}
  {% unless p.category contains "post" %}
   * [{{ p.title }}]({{ p.url | absolute_url }})
  {% endunless %}
{% endfor %}