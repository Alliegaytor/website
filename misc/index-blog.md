---
title: index-blog
layout: index-list
---

<!--
I do not want to look at this markdown abomination ever again
-->

# Index of /blog/

title|creation date|license
:-|:-|--:{%
assign sorted = site.pages | reverse %}{%for p in sorted %}{%if p.category contains "post" %}
[{{ p.title }}]({{ p.url | absolute_url }}) |  {{ p.name | date_to_string }} | {{ p.license }} |{%
endif %}{% endfor %}
{: .dir-table}
