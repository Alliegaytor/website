---
title: 'LUKS Boot Issue'
layout: blog
category: post
tags: linux, encryption, luks, surface laptop
license: 'CC-BY-SA-4.0'
author: Alliegaytor
---

# (Potential) LUKS root filesystem not found on boot fix

Written: 16-01-2024

I had an issue with my Surface Laptop not booting as it couldn't find the encrypted root filesystem after an update (waiting on /dev/mapper/root). I use Arch Linux (btw) and I use systemd-boot and my initramfs used the udev hook to decrypt the drive.

My `mkinitcpio.conf` HOOKS:

```
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems fsck)
```

And my boot loader entry:

```
title   EndeavourOS surface
linux   /vmlinuz-linux-surface
initrd  /intel-ucode.img
initrd  /initramfs-linux-surface.img
options cryptdevice=UUID=****-****-****-****-****:root root=/dev/mapper/root rootflags=subvol=@ rd.luks.options=discard,no-read-workqueue,no-write-workqueue rw
```

To fix it, the first thing I tried was to use the **systemd** and **sd-encrypt** HOOKS as per the Arch Wiki.<sup>[1]</sup> So I edited my `mkinitcpio.conf` to:

```
HOOKS=(base systemd autodetect modconf kms keyboard keymap consolefont block sd-encrypt filesystems fsck)
```

I updated my boot loader entry accordingly:

```
title   EndeavourOS surface
linux   /vmlinuz-linux-surface
initrd  /intel-ucode.img
initrd  /initramfs-linux-surface.img
options rd.luks.name=****-****-****-****-****=root root=/dev/mapper/root rootflags=subvol=@ rd.luks.options=discard,no-read-workqueue,no-write-workqueue rw
```

And now the partition was being detected on boot and it prompted me to unlock it. The problem was that the keyboard was not working anymore (this is a problem with the surface devices,<sup>[2]</sup> the modules needed for the keyboard weren't loaded) and I needed to use a wired keyboard to unlock the root. To fix this, I added these modules to the `mkinitcpio.conf`:

```
MODULES=(surface_aggregator surface_aggregator_registry surface_hid_core intel_lpss intel_lpss_pci 8250_dw surface_kbd)

HOOKS=(base systemd autodetect modconf kms keyboard keymap consolefont block sd-encrypt filesystems fsck)
```

and now it just works.

My drive topology for reference. I have an encrypted btrfs root partition, a non-encrypted /boot and /boot/efi partitions.

<pre>&gt; <font color="#005FD7">lsblk</font>
NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
zram0       253:0    0   5.1G  0 disk  [SWAP]
nvme0n1     259:0    0 238.5G  0 disk  
├─nvme0n1p1 259:1    0   128M  0 part  /boot/efi
├─nvme0n1p2 259:2    0   238G  0 part  
│ └─root    254:0    0   238G  0 crypt /var/log
│                                      /var/cache
│                                      /home
│                                      /
└─nvme0n1p6 259:3    0   384M  0 part  /boot
</pre>

# References

{: .reference-list }
1. [Configuring mkinitcpio - Arch Wiki](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_mkinitcpio) (https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_mkinitcpio)
2. [Linux Surface Github - Disk Encryption](https://github.com/linux-surface/linux-surface/wiki/Disk-Encryption) (https://github.com/linux-surface/linux-surface/wiki/Disk-Encryption)
