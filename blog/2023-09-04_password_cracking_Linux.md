---
title: 'password cracking in Linux'
layout: blog
category: post
tags: linux, encryption, hacker n00b, social anxiety
license: 'CC-BY-SA-4.0'
author: Alliegaytor
---

# password cracking in Linux

Written: 04-09-2023
(Edited: 05-02-2024)

## Preamble
I received an encrypted pdf file from a hospital via email with all my sensitive information in it. Understandably they wanted me to call them for the password and the password wasn't anything like my name or date of birth (this used to be the case for them). However they underestimated my reluctance to phone calls—so I decided to be a little creative with it and tried to crack it with my gpu.

## Cracking
Searching how to crack pdfs online gave me very shady results, but appending "arch linux" to my query seemed to yield decent search results. I stumbled across hashcat and started reading about it.

After learning some of the basics, I found out I needed a hash from the pdf. `pdf2jon` from [John](https://www.openwall.com/john/) did the job:

`pdf2john <pdf-file>`

Which turned out to be a PDF 1.7 Level 8 file (apparently the gold standard of pdf encryption nowadays). I was starting to lose hope in it, but I remembered that the last time I received a similar file from them the password was all digits (0-9) and there were only seven of them. So I came up with this hashcat command to narrow down the possible password permutations.

`hashcat -m 10700 hashes.txt -O -a3 -i --increment-min=7 ?d?d?d?d?d?d?d?d?d?d`

It would try every digit combination between with a minimum of 7 and a maximum of 9 digits. The only issue with it was that this would happen:

```
hiprtcCompileProgram(): HIPRTC_ERROR_COMPILATION
```

It looked like a GPU issue—of which I've had far too many with the RX 5700XT—, so I decided to try the OpenCL backend by using the `--backend-devices` flag, but I was getting a measly 287 H/s.

```
 ╰─λ  hashcat -m 10700 hashes.txt --backend-devices 1 -O -a3 -i --increment-min=7 ?d?d?d?d?d?d?d?d?d?d
...
Speed.#2.........:      287 H/s (280.13ms) @ Accel:1 Loops:1 Thr:256 Vec:1
```

It was going to take hours to try my idea (which might not have even worked). I was going to have to call them...

But! I had put so much time into this already and I wanted to crack a pdf in Arch (btw) to complete the 'Linux hacker' stereotype. I needed to fix the AMD driver issue, and I figured that my AUR installed HIP drivers were *wrong*, so I swapped them out for the fancy new ones from the official arch repos.

I removed opencl-amd and installed rocm:

`sudo pacman -R opencl-amd`
`sudo pacman -S rocm-hip-runtime rocm-opencl-runtime`

Hashcat still threw the error when trying to use HIP with the newly installed drivers :/

````
hiprtcCompileProgram(): HIPRTC_ERROR_COMPILATION
````

It then occurred to me to check what version of hashcat I had installed and check to see if there were any commits after that release. I was running v6.2.6, a release from 23 September 2022 (nearly a year old).

```
 ╰─λ hashcat --version
v6.2.6
```

I found an [issue](https://github.com/hashcat/hashcat/issues/3469), along with it's [closure commit](https://github.com/hashcat/hashcat/commit/eaf3de164bdc327c059d3ae1bd3d2a79f6cd47bd), that seemed to address this very problem. This was from November, the same year, but after the release. So, I needed to compile it myself. I ended up doing the compiling *the lazy way*—thanks to the AUR.

`paru -S hashcat-git hashcat-utils-git`

And now the version installed was:

```
 ╰─λ hashcat --version
v6.2.6-741-g8a3fa5c7d+
```

And when I tried it, it worked!

```
 ╰─λ  hashcat -m 10700 hashes.txt --backend-devices 1 -O -a3 -i --increment-min=7 ?d?d?d?d?d?d?d?d?d?d
hashcat (v6.2.6-741-g8a3fa5c7d+) starting

HIP API (HIP 5.6.31061)
=======================
* Device #1: AMD Radeon RX 5700 XT, 8140/8176 MB, 20MCU

OpenCL API (OpenCL 2.1 AMD-APP.dbg (3570.0)) - Platform #1 [Advanced Micro Devices, Inc.]
=========================================================================================
* Device #2: AMD Radeon RX 5700 XT, skipped

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 16

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
```

I was so caught up in comparing the hashrates between the two runs that when I went back to checkup on it, I thought it had crashed as it wasn't doing anything and my gpu was quiet.... instead it turned out that the reason it wasn't running anymore was that it had cracked the password to the pdf....

It took 38 seconds running my "best guess" command (minimum 7 digits) at a hashrate of 69484 H/s to find the password.

```
Session..........: hashcat
Status...........: Cracked
Hash.Mode........: 10700 (PDF 1.7 Level 8 (Acrobat 10 - 11))
Hash.Target......: $pdf$5*6*256*-1028*1*16*79234b9ac0f286931eac79701b0...554ce9
Time.Started.....: Mon Sep  4 17:26:23 2023 (34 secs)
Time.Estimated...: Mon Sep  4 17:26:57 2023 (0 secs)
Kernel.Feature...: Optimized Kernel
Guess.Mask.......: ?d?d?d?d?d?d?d [7]
Guess.Queue......: 1/4 (25.00%)
Speed.#1.........:    69484 H/s (9.12ms) @ Accel:2 Loops:4 Thr:256 Vec:1
Recovered........: 1/1 (100.00%) Digests (total), 1/1 (100.00%) Digests (new)
Progress.........: 2406400/10000000 (24.06%)
Rejected.........: 0/2406400 (0.00%)
Restore.Point....: 235520/1000000 (23.55%)
Restore.Sub.#1...: Salt:0 Amplifier:4-5 Iteration:60-64
Candidate.Engine.: Device Generator
Candidates.#1....: 9213921 -> 9747707
Hardware.Mon.#1..: Temp: 61c Fan: 25% Util: 99% Core:   0MHz Mem: 875MHz Bus:16

Started: Mon Sep  4 17:26:21 2023
Stopped: Mon Sep  4 17:26:59 2023
```

This is 242 times faster than the OpenCL version that it was using before! Also, wtf is a hospital doing sending 7 digit password encrypted files to people! It was so easy to crack :/

# Conclusion:

Hashcat is a great piece of software for anyone with social anxiety who don't/can't call places to get passwords to open files. It's probably also a great tool for hackers, but I digress.

I went through all this effort to circumvent needing calling someone. Why are phones so scary?
