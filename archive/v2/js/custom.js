// TOGGLE NAVBAR
function navToggle(collapse, hamburger) {
    // Only toggle if visible
    if (getComputedStyle(document.querySelector('.navbar-toggler-icon'), '::after').getPropertyValue('top') !== 'auto') {
        collapse.classList.toggle('show');
        // https://stackoverflow.com/a/72543298
        hamburger.setAttribute(
            'aria-expanded',
            `${!(hamburger.getAttribute('aria-expanded') === 'true')}`
        );
    }
}

// SMOOTHSCROLL
function smoothScroll(elementid, event) {
    event.preventDefault(); // DISABLE LINK
    document.querySelector(elementid).scrollIntoView({
        behavior: 'smooth'
    });
}

// LOAD FUNCTIONS
window.addEventListener('load', () => {
    // CONSTANTS
    const navbar = document.getElementsByClassName('nav-link');
    const collapse = document.getElementById('navbarNav');
    const hamburger = document.getElementsByClassName('navbar-toggler')[0];

    // ADD EVENT LISTENERS
    for (let i = 0; i < navbar.length; i++) {
        const elementid = navbar[i].getAttribute('href');
        if (elementid.startsWith('#')) {
            navbar[i].addEventListener('click', function (event) {
                smoothScroll(elementid, event);
                navToggle(collapse, hamburger);
            });
        }
    }
    hamburger.addEventListener('click', function () {
        navToggle(collapse, hamburger);
    });
});
