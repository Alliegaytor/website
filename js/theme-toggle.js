window.addEventListener('load', () => {
    const themeToggleStyle = document.documentElement.style;

    document.getElementById("toggle-theme").addEventListener('click', () => {
        if (themeToggleStyle.getPropertyValue("--main-bg-color-dark") !== "black") {
            themeToggleStyle.setProperty("--main-bg-color-dark", "black");
        }
        else {
            themeToggleStyle.setProperty("--main-bg-color-dark", "");
        }
    })
})