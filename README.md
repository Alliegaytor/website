---
title: README.md
---

## About

Personal website. Using GitLab pages with jekyll to render markdown to html.

## License

The website source and its pages (and contents) are licensed under the BSD 2-Clause license unless when otherwise specified.

## Project status

In development

## Usage

1. Clone the source code using git
2. Install ruby and jekyll through your package manager of choice
3. Open the directory and run this command:

 ```
        jekyll serve dev
 ```


 [link to website](https://alycat.xyz)
 [source code](https://www.gitlab.com/Alliegaytor/website)
